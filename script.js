//Constants
var URL_BASE = 'https://krono-dev.herokuapp.com/v1/';
var PRODUCTS_TABLE = 'products';
var DATABASE       = 'boom_test';
var ID_STORE       = '54875c925f0496d34cc1ed8f';
var SERVER_PWD     = '1234567890';

//Imports
var mysql   = require('mysql');
var unirest = require('unirest');
var _ = require('underscore');


// Log to firebase
// var date = new Date();
// date = date.getDate() + '-' + (date.getMonth()+1) + '-' + date.getFullYear() + '-' + date.getTime();
// var logCount = 0;
logToServer = function(d) {
    // unirest.post('https://krono-logs.firebaseio.com/boom-pos/' + date + '/logs/'+(logCount++)+'.json')
    //     .header('Accept', 'text/plain').type('json')
    //     .send(JSON.stringify(d));
    console.log(d);
};

var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : DATABASE,
});

connection.connect();

connection.query('SELECT * FROM ' + PRODUCTS_TABLE + ' ORDER BY item', function(err, rows, fields) {
    if (err) throw err;

    console.log('Raw DB Data:\n' + JSON.stringify(rows) + '\n\n\n');

    cleanData(rows);
    var indexed_rows = indexRows(rows);
    console.log('Selected data: ', JSON.stringify(indexed_rows) + '\n\n\n');
    convertUnitsToKGs(indexed_rows);
    applyDiscountFRUVER(indexed_rows);
    console.log('Procesed data: ', indexed_rows);

    unirest.get(URL_BASE + 'stores/' + ID_STORE + '/stock/sync/')
        .type('json')
        .end(function (response) {
            console.log("Prices from server: " + JSON.stringify(response.body) + '\n\n\n');

            var stockItems = {};
            var productCount = 0;

            _(response.body).each(function(stockItem, stockItemStoreId) {
                var row = indexed_rows[stockItemStoreId];

                if(row === null || row === undefined) return;

                var ServerPrice = stockItem.prices[0].value;
                var ServerDiscount = stockItem.prices[0].discount;
                var pushToServer = false;

                if(Number(row.price) != Number(ServerPrice)) {
                    pushToServer = true;
                }

                if(Number(row.discount) != Number(ServerDiscount)) {
                    pushToServer = true;
                }

                if(pushToServer) {

                    stockItems[stockItemStoreId] = {
                        'inventory': -1,
                        'prices': [
                            {
                                'value': Number(row.price),
                                'discount': row.discount
                            }
                        ]
                    };

                    productCount++;
                }
            });

            if(productCount > 0) {
                console.log("Syncronizing " + productCount +  " products.");

                unirest.post(URL_BASE + 'stores/' + ID_STORE + '/stock/sync/')
                    .header('Accept', 'application/json')
                    .type('json')
                    .send({
                        'twoFactorCode': 'XXXYYY',
                        'password': SERVER_PWD,
                        'stockItems': stockItems
                    })
                    .end(function (response) {
                        logToServer("Result " + JSON.stringify(response.body) + '\n');
                    });
                    
                } else {
                    console.log("Succeful sincronization, No products to Syncronize.");
                }
            
        });
});

connection.end();

function getDate(str_date) {
    var year = Number(str_date.slice(0,4));
    var month = Number(str_date.slice(4,6));
    var day = Number(str_date.slice(6,8));
    var date = new Date();
    date.setYear(year);
    date.setMonth(month - 1);
    date.setDate(day);
    return date;
}

function indexRows(rows) {
    var result = {};
    var currentRow = null;
    var currentRowPrices = null;

    _(rows).each(function(row) {
        if(!currentRow || currentRow.item != row.item) {

            if(currentRow) {
                priceDate = getCurrentPrice(currentRowPrices);
                if(priceDate.validity != '20101001') {
                    currentRow.discount = 100 - priceDate.price / currentRow.price * 100;
                }
                currentRow.price = priceDate.price;
                currentRow.validity = priceDate.validity;
            }
            currentRow = row;
            result[row.item] = row;
            currentRowPrices = [];
        }
        currentRowPrices.push({
            price: row.price,
            validity: row.validity
        });
    });
    return result;
}

function cleanData(rows) {
    _(rows).each(function(row) {
        row.description = String(row.descripcion).trim();
        row.brand = String(row.marca).trim();
        row.line = String(row.linea).trim();
        row.active = String(row.activo).trim() == '0';
        row.units = String(row.und_med).trim();
        row.validity = String(row.vigencia).trim();
        row.avg_weight = Number(String(row.peso_aprox).trim()) || 0;
        row.price = Number(String(row.precio).trim()) || -1;
        row.discount = Number(String(row.descuento).trim()) || 0;
        row.inventory = Number(String(row.inventario).trim()) || 0;

        delete row.descripcion;
        delete row.marca;
        delete row.linea;
        delete row.activo;
        delete row.und_med;
        delete row.vigencia;
        delete row.peso_aprox;
        delete row.precio;
        delete row.descuento;
        delete row.inventario;
    });
}

function applyDiscountFRUVER(rows) {
    var discount = 0.35;
    _(rows).each(function(row) {
        // Apply trim to the rows
        if(row.brand == 'FRUVER' && row.line != 'FRUTAS Y VERDURAS SIN discount') {
            row.price *= 1 - discount;
            row.price = Math.ceil(row.price);
            row.discount = 100 - (100 - (row.discount || 0)) * (1-discount);
        }
    });
}

function convertUnitsToKGs(rows) {
    var weightMap = {
        'GRS': 1000,
        'KG': 1,
    };

    _(rows).each(function(row) {
        if(row.units == 'GRS' || row.units == 'KG') {
            row.price *= weightMap[row.units];
            row.units = 'KG';
        }
    });
}

function getCurrentPrice(priceDates) {
    var currentDate = new Date();
    var memo = {
        price: -1,
        validity: new Date(0)
    };
    var result = _(priceDates).reduce(function(memo, priceDate) {
        var date = getDate(priceDate.validity);

        if(date > memo.validity && date < currentDate) {
            return priceDate;
        } else return memo;
    }, memo);

    return result;
}
